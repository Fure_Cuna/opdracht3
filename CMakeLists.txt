cmake_minimum_required(VERSION 3.0)
project(Socket)
add_executable(server server.c)
add_executable(client client.c)
