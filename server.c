#
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <time.h>
#include <ctype.h>

void error(const char * msg) {
	perror(msg);
	exit(1);
}

void Write_To_File(char buffer[]) {
  
  char filename[50];
  time_t tijd = time(NULL);
  struct tm *tm = localtime(&tijd);
  int i=0;
  
  sprintf(filename, "%s.txt",asctime(tm));

  while (filename[i])
  {
	if (isspace(filename[i])) 
		filename[i]='_';
	i++;
  }

  FILE *f = fopen(filename, "w");
  if (f == NULL)
  {
	 printf("Error opening file!\n");
	 exit(1);
  }

  fprintf(f, "%s\n", buffer);

  fclose(f);
}

void Child_Process(int newsockfd) {
  int n;
  int len = 0;
  ioctl(newsockfd, FIONREAD, &len);

  char buffer[len];
  bzero(buffer, len);

    if (len > 0) {
    len = read(newsockfd, buffer, len);
    }
    Write_To_File(buffer);

    if (n < 0) error("ERROR reading from socket");
}

int main(int argc, char * argv[]) {
	
  int sockfd, newsockfd, portno;
  socklen_t clilen;
  pid_t pid;

  struct sockaddr_in serv_addr, cli_addr;
  int n;
  if (argc < 2) {
    fprintf(stderr, "ERROR, no port provided\n");
    exit(1);
  }
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");
  bzero((char * ) & serv_addr, sizeof(serv_addr));

  portno = atoi(argv[1]);

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  if (bind(sockfd, (struct sockaddr * ) & serv_addr,
      sizeof(serv_addr)) < 0)
    error("ERROR on binding");
  listen(sockfd, 5);
  clilen = sizeof(cli_addr);

  while (1) {

    newsockfd = accept(sockfd,
      (struct sockaddr * ) & cli_addr, &
      clilen);

    if (newsockfd < 0)
      error("ERROR on accept");

    else {
      pid = fork();

      if (pid == 0) {
        printf("Bestand ontvangen\r\n");
        close(sockfd);
        Child_Process(newsockfd);
        exit(0);

      } else if (pid > 0) {
        close(newsockfd);
		
      } else if (pid < 0) {
        printf("error");
      }

    }
    if (n < 0) error("ERROR writing to socket");
  }
  close(newsockfd);
  close(sockfd);
  return 0;

}